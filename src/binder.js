/**
 * @param exhange_type
 * Supported routing keys are:
 * @default direct -> unicast routing
 * fanout ->  broadcast routing
 * topic ->  multicast routing
 * headers -> direct exchanges on steroids
 * for detailed explanation refer to https://www.rabbitmq.com/tutorials/amqp-concepts.html
 */
async function bindTopicExchangeToQueue(exchange,binding,queue){
   try {
      await QueueChannel.assertExchange(exchange, 'topic', {durable: true});
      await QueueChannel.assertQueue(queue, {durable: true, autoDelete : true});
      await QueueChannel.bindQueue(queue, exchange, binding);
   } catch (err) {
      throw err
   }
}

async function bindFanoutExchangeToQueue(exchange,binding,queue){
   try {
      await QueueChannel.assertExchange(exchange, 'fanout', {durable: true});
      await QueueChannel.assertQueue(queue, {durable: true, autoDelete : true});
      await QueueChannel.bindQueue(queue, exchange, binding);
   } catch (err) {
      throw err
   }
}

module.exports = {
   bindTopicExchangeToQueue,
   bindFanoutExchangeToQueue,
}