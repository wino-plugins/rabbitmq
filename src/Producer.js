function publishMessage(data){
   try {
      return QueueChannel.publish(data.exchange, data.routing_key, Buffer.from(JSON.stringify(data.payload)), data.options);
   } catch (err) {
      throw err
   }
}

module.exports = {
   publishMessage
}